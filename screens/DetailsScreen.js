import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

export default class DetailsScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Welcome to React Native!</Text>
        <Text >Welcome to your very first android app :)</Text>
        <Text >To get started, edit App.js</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
